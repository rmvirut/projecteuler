
/**
 * Main class of the program provides the primary interface for accessing various sub-programs in thr project
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */

import java.util.*;

public class projectEuler
{
   public static String INTRO = "Welcome to Project Euler in Java by rmvirut. This is a simple student project to improve java literacy and problem solving skill. A mojority of the problems are derived from projecteuler.net.";
    
   public static void main(String[] args){
       Scanner kbd = new Scanner(System.in);
       
       System.out.println(INTRO);
       
       printing.pause();
       kbd.nextLine();
    }
    
    /**
    *If we list all the natural numbers below 10 that are multiples of 3 or 5, we get 3, 5, 6 and 9. The sum of these multiples is 23. The problem described has been modified to include more features. Instea of solving a single
    *issue, it now takes any range of numbers and print out the sum of all multiples of 3 and 5 within said range
    *
    */
    public static void threeAndFive(){
     String intro = "If we list all the natural numbers below 10 that are multiples of 3 or 5, we get 3, 5, 6 and 9. The sum of these multiples is 23." +
     "This program will take any range of numbers and print out the sum of all multiples of 3 and 5 within said range";
     
    printing.pause();
    
    
    
    //Class constants
    int MAXIMUM = Integer.MAX;
    int MINIMUM = Integer.MIN;
    
    int max = min = 0;//set values to max
    
    
    //request input(s) from user
    printing.prompt("Please enter the min or press q to quit: ");
    min = kbd.nextInt();//read int
    kbd.nextLine();//clear input stream
    printing.prompt("Please enter the max or press q to quit: ");
    max = kbd.nextInt();
    kbd.nextLine();

    }
}
