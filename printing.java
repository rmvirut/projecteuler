
/**
 * This class file handles the output of the program
 * 
 * @author rmvirut 
 */
public class printing
{
    /**
     * Prompt the user for "enter" to continue the program 
     */
    public static void pause(){
        System.out.print("Press Enter to continue...");
    }
    
    /**
     * Prints out the specified string as a prompt for the user
     */
    public static void promptUser(String wordPrompt){
       System.out.print(wordPrompt); 
    }
}
